
create table if not exists users (
    tid char(36) primary key,
    email text not null,
    name text
);

create table if not exists games (
    tid char(36) primary key,
    word text not null,
    max_attempt int not null,
    user_tid char(36) references users(tid)
);

create table if not exists rounds (
    word text,
    round_order int,
    game_tid char(36) references games(tid),
    primary key(round_order, game_tid)
)