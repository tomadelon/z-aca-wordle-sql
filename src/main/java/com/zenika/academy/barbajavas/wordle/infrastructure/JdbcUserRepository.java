package com.zenika.academy.barbajavas.wordle.infrastructure;

import com.zenika.academy.barbajavas.WordleApplication;
import com.zenika.academy.barbajavas.wordle.domain.model.users.User;
import com.zenika.academy.barbajavas.wordle.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class JdbcUserRepository implements UserRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public JdbcUserRepository(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void save(User u) {
        //String updateQuery = "users set tid = ?, email=?, name=? ";
        jdbcTemplate.update("INSERT INTO users (tid, email, name) values(?, ?, ? )", u.getTid(), u.getEmail(), u.getUsername());

        //2e methode :

       /** @Override
        public void save(User u) {
            // insert into users(tid, email, name) values(u.tId, u.email, u.name)

            jdbcTemplate.queryForObject("INSERT INTO users(tid, email, name) VALUES(?, ?, ?);", new UserRowMapper(),
                    u.getTid(), u.getEmail(), u.getUsername());
        }*/


    }

    @Override
    public Optional<User> findByEmail(String email) {
        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject("select * from users where email =?", new UserRowMapper(), email ));
        }
        catch (EmptyResultDataAccessException e){
            return Optional.empty();
        }
    }

    @Override
    public Optional<User> findByTid(String userTid) {
        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject("select * from users where tid =?", new UserRowMapper(), userTid ));
        }
        catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public void delete(String userTid) {
        jdbcTemplate.update("delete from users where tid = ?", userTid);


    }

    @Override
    public void update(String userTid, String newUsername){
        jdbcTemplate.update("update users set name=? where tid=?", newUsername, userTid );
    }
}

