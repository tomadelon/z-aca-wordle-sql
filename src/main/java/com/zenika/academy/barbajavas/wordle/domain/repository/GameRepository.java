package com.zenika.academy.barbajavas.wordle.domain.repository;

import com.zenika.academy.barbajavas.wordle.domain.model.game.Game;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class GameRepository {
    private Map<String, Game> games = new HashMap<>();
    
    public void save(Game game) {
        this.games.put(game.getTid(), game);
    }
    
    public Optional<Game> findByTid(String tid) {
        return Optional.ofNullable(games.get(tid));
    }

    public List<Game> findByUserTid(String userTid) {
        return this.games.values().stream()
                .filter(g -> userOwnsGame(userTid, g))
                .collect(Collectors.toList());
    }

    private Boolean userOwnsGame(String userTid, Game g) {
        return g.getUserTid().map(tid -> tid.equals(userTid)).orElse(false);
    }
}
