package com.zenika.academy.barbajavas;

import com.zenika.academy.barbajavas.wordle.domain.model.game.Game;
import com.zenika.academy.barbajavas.wordle.domain.model.users.User;
import com.zenika.academy.barbajavas.wordle.domain.service.i18n.I18n;
import com.zenika.academy.barbajavas.wordle.domain.service.i18n.I18nFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.annotation.PostConstruct;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

@SpringBootApplication
public class WordleApplication {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    @Bean
    public I18n i18n(@Value("${wordle.language}") String language) throws Exception {
        return I18nFactory.getI18n(language);
    }
    
    @Bean
    public Scanner scanner() {
        return new Scanner(System.in);
    }
    
    public static void main(String[] args) throws Exception {
        SpringApplication.run(WordleApplication.class);
    }

    @PostConstruct
    void testJdbc() {

       // System.out.println(jdbcTemplate.queryForObject("select 'Hello, World'", String.class));
        //System.out.println(jdbcTemplate.queryForObject("select * from users where tid =?", new UserRowMapper(), "31f9b408-db93-43a4-9c74-43ca92ee0bdc" ));


    }

    /**private class GameRowMapper implements RowMapper<User> {
        @Override
        public Game mapRow(ResultSet rs, int rowNum) throws SQLException {
            final String gameTid = rs.getString("tid") ;
            final String word = rs.getString("word");
            final Integer maxAttempt = rs.getInt("maxAttempts");

            return new Game(gameTid, word, maxAttempt)

        }
    }*/
}

